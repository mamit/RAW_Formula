# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 07:56:37 2021

@author: mamit
"""

import math
import numpy as np
import xlwings as xw

def mm_raw(lambd, lpp, b, tf, ta, cb, le, lr, dr, kyy,fn):
    """
    Function for computing the R_AW transfer function and its 90% prediction interval
    for arbitrary heading angle following the proposed method by Mittendorf, M.; Nielsen,
    U.D.; Bingham, H.B.; Liu, S. (2021) "Towards the Uncertainty Quantification of Semi-Empirical
    Formulas Applied to the Added Resistance of Ships in Waves of Arbitrary Heading" (under review).


    INPUT 
    wl - wavelength [m]
    lpp - length per perpendicular [m]
    b - beam [m]
    tf - draught at fp [m]
    ta - draught at ap [m]
    cb - block coefficient [-]
    le - length of run (bow) [m]
    lr - length of run (stern) [m]
    kyy - norm. radius of gyration (kyy/lpp), in general 0.25 [-]
    fn - froude number [-]

    OUTPUT
    array[0] - added resistance coefficient (mean)
    array[1] - added resistance coefficient (lower)
    array[2] - added resistance coefficient (upper)
    array[3] - intrinsic wave frequency
    array[4] - normalized wave frequency(w/sqrt(g/lpp))
    array[5] - lambda/lpp
    """

    #initalization
    alp = math.radians(dr)
    tmax = max(tf, ta)
    delta = ta-tf
    g = 9.807
    raw_arr = []
    U = fn*math.sqrt(g*lpp)
    E1 = math.atan(0.99*b*0.5/le)
    E2 = math.atan(0.99*b*0.5/lr)

   #old 
    """ps1 =  [3.99113498e+00, 7.05000000e-01, 6.63880941e+01, 1.00000013e+00,
 3.70884073e+00, 6.50000000e-01, 2.25000005e+00, 1.37681243e-01,
 1.63644559e+00, 3.70769079e+00, 3.59999855e+01, 1.04378084e+01,
 8.30985178e+00, 5.54671271e+02, 2.67392434e+00, 1.07683340e+02,
 1.64141385e+00, 8.32000004e-02, 3.50000000e+00, 7.88938409e-01,
 1.25000025e-01, 1.03000003e+00, 1.25156489e+00, 6.47734924e-01,
 1.12599961e+01, 4.99999985e+00, 3.15000000e+00, 3.86777168e+00,
 7.48062172e+00]

  
    ps2 =[4.12300201e+00, 7.06427000e-01, 7.18576583e+01, 1.08208800e+00,
 4.54276337e+00, 6.498110000e-01, 2.47876172e+00, 2.09999164e-01,
 1.48139390e+00, 3.68723319e+00, 2.85170659e+01, 8.19615000e+00,
 6.37000001e+00, 4.25001839e+02, 3.49914090e+00, 1.33721082e+02,
 1.94954005e+00, 8.84094641e-02, 3.51005409e+00, 7.01003750e-01,
 1.25007042e-01, 1.03000189e+00, 1.18385036e+00, 6.25613420e-01,
 1.21426629e+01, 5.12014108e+00, 3.14999895e+00, 1.00000003e+00,
 7.49996389e+00]
    ps3 = [3.07823077e+00, 4.178612e-01, 5.03809738e+01, 1.49465996e+00,
 3.85610768e+00, 8.66233109e-01, 3.28965170e+00, 1.18934002e-01,
 1.58734589e+00, 2.62011640e+00, 3.30582105e+01, 1.32459469e+01,
 1.06778637e+01, 7.01471393e+02, 1.95035154e+00, 1.07553181e+02,
 2.32509244e+00, 1.34303440e-01, 2.97601813e+00, 7.88335698e-01,
 1.49781250e-01, 1.37234629e+00, 1.43760490e+00, 5.89491425e-01,
 1.76219025e+01, 2.91906915e+00, 3.16103907e+00, 6.01272301e+00,
 1.97630147e+00]

    pb1 = [5.80971830e+00, 4.19695236e-01, 9.02109565e+01, 1.74143530e+00,
 2.97128871e+00, 8.99999633e-01, 3.90000579e+00, 2.69873938e-01,
 1.36006613e+00, 4.41611521e+00, 2.99601645e+01, 1.23160683e+01,
 5.78735744e+00, 5.61862817e+02, 2.39745748e+00, 1.60655769e+02,
 1.87132118e+00, 6.56497145e-02, 6.22927196e+00, 7.48692520e-01,
 2.61288651e-01, 1.97955200e+00, 1.30731728e+00, 8.39267532e-01,
 1.71518407e+01, 3.08488696e+00, 3.49100473e+00, 3.12146938e+00,
 4.42577460e+00]
    pb2 = [4.52283290e+00, 6.14413201e-01, 5.42667529e+01, 1.26307383e+00,
 4.53785774e+00, 7.90585719e-01, 2.87913251e+00, 2.05127596e-01,
 1.50521221e+00, 2.90848212e+00, 2.78638125e+01, 1.18539317e+01,
 6.37000021e+00, 5.14663510e+02, 3.48079683e+00, 1.17950074e+02,
 1.99312593e+00, 1.07166845e-01, 2.41847948e+00, 7.18781216e-01,
 1.67384833e-01, 1.13378237e+00, 1.27472765e+00, 6.33400538e-01,
 1.32158328e+01, 3.00213197e+00, 2.81009795e+00, 1.00096514e+00,
 7.49999998e+00]
     #lower
    pb3 = [3.20910888e+00, 4.20121e-01, 5.69818847e+01, 1.23818358e+00,
 4.23636241e+00, 1.09000000e+00, 3.74999487e+00, 1.20121250e-01,
 1.73388473e+00, 4.39788428e+00, 3.13490311e+01, 1.13187778e+01,
 1.06934598e+01, 6.33408876e+02, 2.11744482e+00, 1.18953274e+02,
 2.09405032e+00, 8.32000104e-02, 3.07894251e+00, 7.33804923e-01,
 2.08506761e-01, 1.21743687e+00, 1.26590636e+00, 7.43740319e-01,
 1.73351721e+01, 2.99000023e+00, 2.40527456e+00, 6.01000000e+00,
 2.02000002e+00]"""
#mean
    ps1= [4.05946942e+00, 7.04999991e-01, 5.62274820e+01, 1.18458738e+00,
 4.05706220e+00, 6.50000001e-01, 2.25000391e+00, 1.33566557e-01,
 1.62572344e+00, 3.58496857e+00, 2.91066129e+01, 1.08675480e+01,
 9.87827505e+00, 4.80961585e+02, 2.62009924e+00, 1.23162591e+02,
 1.74527790e+00, 1.06966785e-01, 3.50000000e+00, 8.24976128e-01,
 1.25000000e-01, 1.03000141e+00, 1.20135494e+00, 7.22951370e-01,
 1.36307675e+01, 5.00000000e+00, 3.14999975e+00, 2.89714558e+00,
 5.55186351e+00]

#upper
    ps2 = [4.27240317e+00, 7.05000000e-01, 5.58810360e+01, 1.05849695e+00,
 5.00000000e+00, 6.50000000e-01, 2.53759425e+00, 2.10000000e-01,
 1.50221848e+00, 3.58270417e+00, 2.70758743e+01, 8.20000054e+00,
 6.37000001e+00, 4.25000004e+02, 3.50000000e+00, 1.21801494e+02,
 2.04089706e+00, 8.35524201e-02, 3.50000000e+00, 6.39449863e-01,
 1.25000223e-01, 1.03000050e+00, 1.13419070e+00, 6.03042936e-01,
 1.23638297e+01, 5.00000000e+00, 3.14999947e+00, 1.00000004e+00,
 7.49999988e+00]
#lower

    ps3 = [3.03271576e+00, 4.20000015e-01, 4.99673118e+01, 1.65946595e+00,
 3.46566078e+00, 1.08999872e+00, 3.53017296e+00, 1.20000000e-01,
 1.48347334e+00, 2.62015273e+00, 3.09243793e+01, 1.29737533e+01,
 1.07000000e+01, 7.07939677e+02, 1.95001682e+00, 9.35351064e+01,
 2.16620326e+00, 1.39999997e-01, 2.89553627e+00, 8.24992364e-01,
 1.30763658e-01, 1.03000000e+00, 1.42153913e+00, 6.31753054e-01,
 1.72855270e+01, 3.00000001e+00, 3.11414443e+00, 6.00000000e+00,
 2.00000001e+00]


    pb1 = [6.98574593e+00, 4.25032242e-01, 7.09177633e+01, 1.99960254e+00,
 3.01301221e+00, 8.99994676e-01, 3.72568388e+00, 2.81612167e-01,
 1.38750811e+00, 3.86896582e+00, 1.79628922e+01, 1.14985385e+01,
 5.58870161e+00, 4.73743004e+02, 2.21408408e+00, 1.53646073e+02,
 2.43280559e+00, 5.56757940e-02, 2.41039849e+00, 8.13139297e-01,
 2.99999998e-01, 1.00776665e+00, 1.07619998e+00, 7.78406778e-01,
 1.92414808e+01, 3.02938365e+00, 3.84472339e+00, 3.12300755e+00,
 4.52696031e+00]

    pb2 = [4.58710582e+00, 5.65022282e-01, 5.42786254e+01 ,1.23604172e+00,
 4.99998263e+00, 6.50259977e-01, 2.25000468e+00, 2.10000000e-01,
 1.54949896e+00, 3.95366005e+00, 3.37772648e+01, 1.39999578e+01,
 6.37000001e+00, 4.25000564e+02, 3.50000000e+00, 1.18022195e+02,
 2.04252813e+00, 1.04536091e-01, 2.26795773e+00, 7.01539307e-01,
 1.25034505e-01, 1.03001307e+00, 1.44950225e+00, 6.47384026e-01,
 1.44229162e+01, 3.00000134e+00, 3.14999949e+00, 1.00000000e+00,
 7.49999997e+00]


    pb3 = [3.00355039e+00, 4.20001279e-01, 5.58414825e+01, 1.32934752e+00,
 4.31245215e+00, 1.08998811e+00, 3.74999578e+00, 1.46428490e-01,
 1.79301192e+00, 3.56562762e+00, 2.76275045e+01, 1.22975415e+01,
 1.06999891e+01, 6.79271138e+02, 2.30875016e+00, 1.30001874e+02,
 2.23066085e+00, 8.32000024e-02, 2.63843128e+00, 7.83843682e-01,
 2.20011546e-01, 1.35089575e+00, 1.31843660e+00, 6.26778792e-01,
 1.52590637e+01, 3.00000000e+00, 2.40826045e+00, 5.99999925e+00,
 2.00000000e+00]
    
    raw_arr = []
    
    if cb <0.7:
        pv = [ps1, ps2, ps3]
    else:
        pv = [pb1, pb2, pb3]
    #loop for all 3 estimates
    for i in range(3): 
        p = pv[i]
        raw_local = []
        if i ==0:
            add = 0
        elif i==1:
            add = 0.19967
        else:
            add =-0.19967
        if fn<0.12:
            a2 = (0.0072+p[7]*fn)
        else:
            a2 = (fn**p[8]*math.exp(-p[9]*fn))
        a3 = (1.+p[10]*math.atan((abs(delta))/lpp))
         
        w0 = math.sqrt((2*math.pi*g)/lambd)
        Vc = math.sqrt(g*lambd/2/math.pi)
        
        omega = (p[16]*(kyy**(1/3))*math.sqrt(lpp/lambd)*(1-(p[17]/cb)*(math.log(b/tmax)-math.log(p[18])))*(cb/p[19])**p[20]*((-p[21]*fn**2+p[22]*fn)*abs(math.cos(alp))+(p[23]*(13+math.cos(2*alp)))/p[24]))
        if omega < 1:
            b1 = p[11]
        else:
            b1 = -p[12]

        if omega < 1:
            d1 = p[13]*(lpp*cb/b)**(-p[14])
        else:
            d1 = -p[13]*(lpp/b)**(-p[14])*(4-p[15]*math.atan2(abs(delta),lpp))
    #f(alpha)
        if math.degrees(math.pi -E1) <= dr and dr<= math.degrees(math.pi):
            fofa = -math.cos(alp)
        elif alp < math.pi -E1:
            fofa = 0

        a1 = p[2]*cb**p[3]*(p[4]*kyy)**2*(p[5]/cb)**(-(1+fn)*math.cos(alp))*math.log(b/tmax)**(-1)*(1-2*math.cos(alp))/p[6]
        a90 = p[2]*cb**p[3]*(p[4]*kyy)**2*(p[5]/cb)**(-(1+fn)*math.cos(0.5*math.pi))*math.log(b/tmax)**(-1)*(1-2*math.cos(0.5*math.pi))/p[6]
    #for following waves
        if U<(Vc/2):
            a20 = (-0.0072+0.0072*4/Vc*U) *(0.87/cb)
        elif (U-Vc/2)/math.sqrt(g*lpp)<=0.12:
            a20 = (0.0072+p[7]*(U-Vc/2)/math.sqrt(g*lpp)) *(0.87/cb)
        else:
            a20 = ((U-Vc/2)/math.sqrt(g*lpp))**1.5*math.exp(-3.5*(U-Vc/2)/math.sqrt(g*lpp))*(0.87/cb)**(1+(U-Vc/2)/math.sqrt(g*lpp))
        fnr = (U-Vc/2)/math.sqrt(g*lpp)
        a10 = p[2]*cb**1.34*(4*kyy)**2*math.log(b/tmax)**(-1)

        if dr == 0:
            rawm = p[0]*a10*a20*a3*omega**(b1)*math.exp((b1/d1)*(1-omega**(d1)))
        elif dr>0 and dr <90:
            rawm0 = p[0]*a90*a2*a3*omega**(b1)*math.exp((b1/d1)*(1-omega**(d1)))
            rawm1 = p[0]*a10*a20*a3*omega**(b1)*math.exp((b1/d1)*(1-omega**(d1)))
            rawm = rawm0+(rawm1-rawm0)*(1-dr/90)
        else:
            rawm = p[0]*a1*a2*a3*omega**(b1)*math.exp((b1/d1)*(1-omega**(d1)))
    #alpha & tstar

        tstar= tmax
        if lambd/lpp <= 2.5:
            alpha12 = 1-math.exp(-p[25]*math.pi*(tstar/lambd - tstar/(p[26]*lpp)))
        else:
            #alpha12 = 0
            alpha12 = 1-math.exp(-p[25]*math.pi*(tstar/lambd - tstar/(p[26]*lpp)))


        tstar= (tmax*(p[27]+math.sqrt(abs(math.cos(alp)))))/p[28]

        if lambd/lpp <= 2.5:
            alpha34 = 1-math.exp(-p[25]*math.pi*(tstar/lambd - tstar/(p[26]*lpp)))
        else:
            #alpha34 = 0
            alpha34 = 1-math.exp(-p[25]*math.pi*(tstar/lambd - tstar/(p[26]*lpp)))
    #segment 1
        if math.degrees(E1) <= dr and dr<= math.degrees(math.pi):
            rawr1= p[1]*(lpp/b)*alpha12*((math.sin(E1-alp)**2)+2*w0*U/g*(math.cos(E1)*math.cos(E1-alp)-math.cos(alp)))*(0.87/cb)**((1+4*math.sqrt(fn))*fofa)
        else:
            rawr1 = 0
    #segment 2
        if math.degrees(math.pi-E1) <= dr and dr<= math.degrees(math.pi):
            rawr2= p[1]*(lpp/b)*alpha12*((math.sin(E1+alp)**2)+2*w0*U/g*(math.cos(E1)*math.cos(E1+alp)-math.cos(alp)))*(0.87/cb)**((1+4*math.sqrt(fn))*fofa)
        else:
            rawr2 = 0
    #segment 3
        if 0 <= dr and dr<= math.degrees(math.pi-E2):
            rawr3= -p[1]*(lpp/b)*alpha34*((math.sin(E2+alp)**2)+2*w0*U/g*(math.cos(E2)*math.cos(E2+alp)-math.cos(alp)))
        else:
            rawr3 = 0
    #segment 4          
        if 0 <= dr and dr<=math.degrees(E2):  
            rawr4= -p[1]*(lpp/b)*alpha34*((math.sin(E2-alp)**2)+2*w0*U/g*(math.cos(E2)*math.cos(E2-alp)-math.cos(alp)))
        else:
            rawr4 = 0
        rawr = rawr1 + rawr2 +rawr3 +rawr4
        raw =rawr+rawm
        raw_arr.append(raw+add)
        #raw_arr.append(raw_local)
        #define output array
    w = np.sqrt(np.divide(2*math.pi*g,lambd))
    wtilde = np.divide(w, math.sqrt(g/lpp))
    ll = np.divide(lambd, lpp)
    ret_arr = [raw_arr[0],raw_arr[1],raw_arr[2],w, wtilde, ll]
    return ret_arr

@xw.func
def mean_caw(lambd, lpp, b, tf, ta, cb, le, lr, dr, kyy,fn):
    #lamdb = lambd*lpp
    array = mm_raw(lambd, lpp, b, tf, ta, cb, le, lr, dr, kyy,fn)
    return array[0]

@xw.func
def lower_caw(lambd, lpp, b, tf, ta, cb, le, lr, dr, kyy,fn):
    #lamdb = lambd*lpp
    array = mm_raw(lambd, lpp, b, tf, ta, cb, le, lr, dr, kyy,fn)
    return array[2]
@xw.func
def upper_caw(lambd, lpp, b, tf, ta, cb, le, lr, dr, kyy,fn):
    #lamdb = lambd*lpp
    array = mm_raw(lambd, lpp, b, tf, ta, cb, le, lr, dr, kyy,fn)
    return array[1]


