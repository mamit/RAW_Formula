# ![](testcase.png) A Semi-Empirical Formula for the Prediction of the Added Resistance and its Uncertainty in Arbitrary Wave Heading

<div align="justify">

The Python function is placed in "raw_formula.py" and the Excel implementation is in the eponymous directory. A number of examples are given in the IPython notebook "test.ipynb" and in the Excel worksheet.
A Python 3 compiler and the libraries numpy, matplotlib as well as xlwings are required. User defined functions (UDF) only work in Excel in the Windows implementation. In case of another OS, please use the Python file. For License and Disclaimer information please have look into the LICENSE file. It is stressed that we do not take any responsibility or warranty and that you use the code at your own risk. 
For questions consult the paper "Towards the Uncertainty Quantification of Semi-Empirical Formulas Applied to the Added Resistance of Ships in Waves of Arbitrary Heading" and for error/bug reporting contact the corresponding author. 

<div align="justify">
Written by: Malte Mittendorf(1), Ulrik Dam Nielsen(1), Harry B. Bingham(1) & Shukui Liu(2)
<div align="justify">
(1) DTU Mechanical Engineering, Technical University of Denmark
<div align="justify">
(2) School of Mechanical and Aerospace Engineering, Nanyang Technological University, Singapore

<div align="justify">
The financial support from The Danish Maritime Fund (Projekt 2019-043), Orients Fond and Department of Mechanical Engineering, Technical University of Denmark is highly appreciated. The second author has received funding by the Research Council of Norway through the Centres of Excellence scheme, project number 223254 AMOS. 

